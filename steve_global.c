#include "steve_global.h"

static char breadcrumb_string[128] = "hurr";
char * Steve_GLOBAL_GetBreadcrumbString(void)
{
  return breadcrumb_string;
}


//must be a zero terminated string
void Steve_GLOBAL_CopyBreadcrumbString(char * str){
  
  for(int i=0; i<128;i++){
    if (str[i] == 0){
      return;
    }
    breadcrumb_string[i] = str[i];
  }
}
