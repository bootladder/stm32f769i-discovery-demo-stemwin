#include "steve_uart.h"

#include "stm32f7xx_hal.h"

static HAL_StatusTypeDef init_status = STEVE_UART_INIT_STATUS_NOTHING;

static UART_HandleTypeDef huart = {};


STEVE_UART_INIT_STATUS_t Steve_UART_GetInitStatus(){ return init_status; }

const char * Steve_UART_GetInitStatusString(){
  switch(init_status){
     case HAL_OK      :
       return "OK!";
     case HAL_ERROR   :
       return "ERROR!";
     case HAL_BUSY    :
       return "BUSY!";
     case HAL_TIMEOUT :
    return "TIMEOUT!";
  }
  return "wat";
}

void Steve_UART_Init(void)
{

  //init
  huart.Instance = UART5;
  huart.Init.HwFlowCtl = UART_HWCONTROL_NONE;

  //config

  huart.Init.BaudRate = 115000;
  huart.Init.WordLength = UART_WORDLENGTH_8B;
  huart.Init.StopBits = UART_STOPBITS_1;
  huart.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;

  huart.Init.Parity = UART_PARITY_NONE;
  huart.Init.Mode = UART_MODE_TX_RX;
  //oversampling skipped

  init_status = HAL_UART_Init(&huart);

}
