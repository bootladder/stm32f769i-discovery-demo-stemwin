
typedef enum {
  STEVE_UART_INIT_STATUS_NOTHING
}STEVE_UART_INIT_STATUS_t ;

STEVE_UART_INIT_STATUS_t Steve_UART_GetInitStatus();
const char * Steve_UART_GetInitStatusString();

void Steve_UART_Init(void);
